# Korean Books and Helmet Project

## 시작: 권혁남 박사님
[here](https://www.facebook.com/photo.php?fbid=2736071749803104&set=a.114964421913863&type=3&theater)

## 기존 문제점 
- 한국어 책 검색이 실질적으로 불가함
- 도서관 사용자 기준:
    - 한국어 책제목 및 저자명이 `Suomi Alphabet`으로 음차되어 있음    
    - 한국어 책제목 및 저자명으로 검색 불가 
- 도서관 관리자 기준: 한국어를 모르는 Helmet의 관리자의 한국어책 제목 및 저자 관리가 어려울 것으로 예상  

## 의의
- 도서관 사용자 기준 : 핀란드 수도권 내 거주중인 어린이들의 한국 어린이를 위한 책들의 검색 및 예약가능
- 사용자 관리자 기준 : 핀란드 수도권 내 Helmet에서 구매, 관리하는 한국도서들의 실질적 운용 가능

## 문제 예시
[here](https://haku.helmet.fi/iii/encore/search/C__Statu__Ff%3Afacetlanguages%3Akor%3Akor%3AKorean%3A%3A__Orightresult__U__X0?lang=eng&suite=cobalt)
![sample image](helmet_search_example.png)

## 방안1 books.google.com
- 판매되는 책들은 모두 고유한 `ISBN 코드`가 있다고 가정한다면, 아래와 같이 작동하는 작은 스크립트 혹은 앱을 만들어 Helmet.fi 에 제공
	- 입력: ISBN코드 
	- 출력: 한국어 제목, 저자

## 방안1 예시 
[here](https://books.google.fi/books?id=B0JMmwEACAAJ&dq=9788993577754&hl=en&sa=X&ved=0ahUKEwizk5j1j57mAhX_AxAIHebiD9cQ6AEIKTAA)
![sample image2](books_google_search_example.png)

## 방안1 문제점
- 놀랍게도? `https://books.google.com`가 모든 한국어 책의 ISBN정보를 찾지 못함
- `https://isbnsearch.org/`에서 찾아지나 제목과 저자가 영어로 나옴

## 방안2 seoji.nl.go.kr
[here](http://seoji.nl.go.kr/landingPage/SearchList.do)
![sample image3](seoji_example.png)
![sample image4](seoji_example2.png)
- 국립중앙도서관에서 제공하는 `서지정보유통지원시스템` 이라는 곳에서 `ISBN 코드`를 통해 대부분의? 한국어 제목 및 저자 출력이 가능한 것으로 보임
- 표제/저자사항 내 입력이 일정하지 않아 `/`로만 분리
	- ex) 표제/저자사항: (타투와 파투)여기는 핀란드/ 글.그림 : 아이노 하부카이넨,사미 토이보넨; 옮김: 이지영
	- ex) 표제/저자사항: 무민 골짜기의 모험 1/ 원작:토베 얀손; 천미나 옮김

## 파이썬 Script
[here](https://gitlab.com/finko-dev/tech-club/koreanbooks_helmet_project/blob/master/Korean_Helmet.py)


## 느낀점
- `구글`이 아직 만능은 아님