
from bs4 import BeautifulSoup
import urllib.request as req
import urllib.parse as rep
import sys
import io

sys.stdout = io.TextIOWrapper(sys.stdout.detach(), encoding ='utf-8')
sys.stderr = io.TextIOWrapper(sys.stderr.detach(), encoding ='utf-8')

base = "http://seoji.nl.go.kr/landingPage?isbn="
# ISBN code
quote = rep.quote_plus("9788934996224") 
url = base + quote

res = req.urlopen(url)
soup = BeautifulSoup(res, "html.parser")
test = soup.select("#contents > div.viewBox > div.bookBox > div.infoArea.thumb_none > div > dl")


txt = soup.find("dd").string
txt_splited = txt.split("/")
title = txt_splited[0]
author= txt_splited[1]

print("Title:" , title)
print("Author:", author)

